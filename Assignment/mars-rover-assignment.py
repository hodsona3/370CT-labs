from threading import Thread, Condition
import time, threading, random

cv = threading.Condition()

global access, wheels, problem, solution, log, wheel, probSolved, distanceLeft, probFound

access = 0

distanceLeft = 0.0

probSolved = False

#State for all the wheels - Will be changed throughout the process of the program
wheelState = [ 'Working', 'Working', 'Working', 'Working', 'Working', 'Working' ]

#Problems will be selected from this list for the Mars Rover to attempt to solve
problemList = [ 'Blockage', 'Sinking Wheel', 'Freewheeling' ]

#A list of possible solutions to the problems above
solution = [  'Raising wheel', 'Lowering Wheel', 'Asking for help']

#Logical reaction for the Rover to do after solving a problem
action = [ 'Lowering Wheel', 'Raising Wheel' ]

log = []

#Critical Section
def condition():
    
    global access, wheel, probSolved, problem, distanceLeft, probFound
    
    cv.acquire()
    
    #Movement
    if( access == 0 ):
        
        print( '\n' )
        for x in range( 0, 100 ):
            
            print( "Moving..." )
            
            distanceLeft += 0.2
            distanceLeft = round( distanceLeft, 1 )

            print( 'Distance Travelled: {}m'.format( distanceLeft ) )
            
            time.sleep( .3 )
            #Generates a random number to decide if there is a problem
            problem = random.randint( 1, 6 )
            
            #If there is a problem
            if( problem < 3 ):
                print( '\n' )
                
                access = 1
                probSolved = False
                probFound = True
                break
            
        time.sleep(3)
        cv.notifyAll()
        cv.release()
    
    #Brain
    elif( access == 1 ):
        
        #If there is a problem
        if( probFound ):
            #Stops all wheels from moving
            for i in range( len( wheelState ) ):
                wheelState[ i ] = "Stopped"
            
            #Choices a problem at random
            problem = random.choice( problemList )
            
            #The problem applies to multiple wheels
            if( random.choice( [True, False] ) ):
                #Identifies the amount of wheels problem applies to
                wheelNum = random.randint( 2, 6 )
            #The problem applies to a single wheel
            else:
                wheelNum = 1

            #Identifies the wheel(s) the problem applies to
            wheel = random.sample( range( 0, 6 ), wheelNum )
            
            #Blockage
            if( problemList.index( problem ) == 0 ):
                #Changes the state of the wheel(s) to blocked
                for x in range( len( wheel ) ):
                    block = wheel[ x ]
                    wheelState[ block ] = "Blocked"
                    access = 2

            #Sinking 
            elif( problemList.index( problem ) == 1 ):
                #Changes the state of the wheel(s) to sinking
                for x in range( len( wheel ) ):
                    sink = wheel[ x ]
                    wheelState[ sink ] ="Sinking"
                    access = 4
            
            #Freewheeling
            elif( problemList.index( problem ) == 2 ):
                #Changes the state of the wheel(s) to freewheeling
                for x in range( len( wheel ) ):
                    free = wheel[ x ]
                    wheelState[ free ] = "Freewheeling"
                    access = 3

            print( "{} encountered by {} wheel(s)".format( problem, len( wheel ) ) )
            
            print( wheelState )    
            
            #If the amount of wheels affected is greater that 2 then the problem
            #is too hard
            if( wheelNum > 2 ):
                print( "Problem is too hard" )
                access = 5

            probFound = False
            
        #If a problem is solved
        elif( probSolved ):
            
            #Tells all the wheels problem is fixed  
            for i in range( len( wheelState ) ):
                wheelState[ i ] = "Working"
            
            #Moves Robot on after being blocked
            if( problemList.index( problem ) == 0 ):
                print( 'Moving...' )
              
                distanceLeft += 0.1
                print( action[ 0 ] )

            #Moves Robot on after sinking
            elif( problemList.index( problem ) == 1 ):
                print( 'Moving...' )
                
                distanceLeft += 0.1
                print( action[ 0 ] )
                
            #Moves Robot on after freewheeling
            elif( problemList.index( problem ) == 2 ):
                print( 'Moving...' )
                
                distanceLeft += 0.1
                print( action[ 1 ] )
            
            access = 0
        
        #Sends for help  
        else: 
            print( 'Direction is bad. Pick a new one' )
            
            #Send error report for blockage
            if( problemList.index( problem ) == 0 ):
                #Logs that the Rover required assistance
                log.append( '#{} - Blockage - Asked For Help'.format( len( log ) + 1 ) )

            #Moves Robot on after sinking
            elif( problemList.index( problem ) == 1 ):
                #Logs that the rover required assistance
                log.append( '#{} - Sinking Wheel - Asked For Help'.format( len( log ) + 1 ) )

            #Moves Robot on after freewheeling
            elif( problemList.index( problem ) == 2 ):
                #Logs that the rover required assistance
                log.append( '#{} - Freewheeling - Asked For Help'.format( len( log ) + 1 ) )
                
            #Tells all the wheels problem is fixed  
            for i in range( len( wheelState ) ):
                wheelState[ i ] = "Working"
                
            probSolved = True
            access = 0
        
        time.sleep(3)
        cv.notifyAll()
        cv.release()
    
    #Blockage    
    elif( access == 2 ):
        
        val = solution[ 0 ]
        print( 'Attempting to solve by {}'.format( val ) )
        
        time.sleep( 3 )
        #decides at random if the problem is solved        
        attempt = random.choice( [True, False] )
        
        #If attempt was successful 
        if( attempt ):
            print( 'Problem solved' )

            #Logs that the rover solved the problem
            log.append( '#{} - Blockage - {} - Solved'.format( len( log ) + 1, val ) )
            probSolved = True
        
        #If it wasn't successful, get help
        else:
            print( 'Problem not solved' )
            
            #Logs that the rover solved the problem
            log.append( '#{} - Blockage - {} - Not Solved'.format( len( log ) + 1, val ) )
            probSolved = False
            
        #Move back to 'brain' to send an help requst
        access = 1
        time.sleep(3)
        cv.notifyAll()
        cv.release()
    
    #Freewheeling
    elif( access == 3 ):
        val = solution[ 1 ]
        print( '\nAttempting to solve by {}'.format( val ) )
        
        time.sleep( 3 )
        #Decides at random if the problem is solved
        attempt = random.choice( [True, False ])
        
        #If attempt was successful
        if( attempt ):
            print( '\nProblem Solved' )
            
            #Logs that the rover solved the problem
            log.append( '#{} - Freewheeling - {} - Solved'.format( len( log ) + 1, val ) )
            probSolved = True
        
        #If attempt was unsuccessful, get help   
        else:
            print( '\nProblem not solved' )

            #Logs that the rover didn't solved the problem
            log.append( '#{} - Freewheeling - {} - Not Solved'.format( len( log ) + 1, val ) )
            probSolved = False
        
        
        #Moves back to 'brain' to alert all wheels problem is solved
        access = 1
        time.sleep(3)
        cv.notifyAll()
        cv.release()
    
    #Sinking
    elif( access == 4 ):
        val = solution[ 0 ]
        print( '\nAttempting to solve by {}'.format( val ) )
        
        time.sleep( 3 )
        #Decides at random if problem is solved
        attempt = random.choice( [True, False] )
        
        #If attempt was successful
        if( attempt ):
            print( '\nProblem solved' )
            
            #Logs that the rover solved the problem
            log.append( '#{} - Sinking Wheel - {} - Solved'.format( len( log ) + 1, val ) )
            probSolved = True
        
        #If attempt was unsuccessful, get help   
        else:
            print( '\nProblem not solved' )
            
            #Logs that the rover solved the problem
            log.append( '#{} - Sinking Wheel - {} - Not Solved'.format( len( log ) + 1, val ) )
            probSolved = False
        
        #Move back to 'brain' to send an help requst
        access = 1    
        time.sleep(3)
        cv.notifyAll()
        cv.release()
    
    #Just to keep each section at an equal level of threads
    else:
        access = 1
        time.sleep(3)
        cv.notifyAll()
        cv.release()

#Initiats the movement
def t1():
    condition()
#Enters brain, stops the wheels and identifies the problem
def t2():
    condition()
#Attempts to solve problem
def t3():
    condition()
#Moves back into brain 
def t4():
    condition()
#Movement
def t5():
    condition()
#Enters brain, stops the wheels and identifies the problem
def t6():
    condition()
#Attempts to solve problem
def t7():
    condition()
#Moves back into brain 
def t8():
    condition()
#Movement
def t9():
    condition()
#Enters brain, stops the wheels and identifies the problem
def t10():
    condition()
#Attempts to solve problem
def t11():
    condition()
#Moves back into brain 
def t12():
    condition()
#Movement
def t13():
    condition()
#Enters brain, stops the wheels and identifies the problem
def t14():
    condition()
#Attempts to solve problem
def t15():
    condition()
#Moves back into brain 
def t16():
    condition()
#Movement
def t17():
    condition()
#Enters brain, stops the wheels and identifies the problem
def t18():
    condition()
#Attempts to solve problem
def t19():
    condition()
#Moves back into brain 
def t20():
    condition()

t1 = Thread( target=t1, args=() )
t2 = Thread( target=t2, args=() )
t3 = Thread( target=t3, args=() )
t4 = Thread( target=t4, args=() )
t5 = Thread( target=t5, args=() )
t6 = Thread( target=t6, args=() )
t7 = Thread( target=t7, args=() )
t8 = Thread( target=t8, args=() )
t9 = Thread( target=t9, args=() )
t10 = Thread( target=t10, args=() )
t11 = Thread( target=t11, args=() )
t12 = Thread( target=t12, args=() )
t13 = Thread( target=t13, args=() )
t14 = Thread( target=t14, args=() )
t15 = Thread( target=t15, args=() )
t16 = Thread( target=t16, args=() )
t17 = Thread( target=t17, args=() )
t18 = Thread( target=t18, args=() )
t19 = Thread( target=t19, args=() )
t20 = Thread( target=t20, args=() )

print( '\nMission: Find and Analyse a Basalt Rock' )
time.sleep( 2 )
t1.start()
t2.start()
t3.start()
t4.start()
t5.start()
t6.start()
t7.start()
t8.start()
t9.start()
t10.start()
t11.start()
t12.start()
t13.start()
t14.start()
t15.start()
t16.start()
t17.start()
t18.start()
t19.start()
t20.start()


t20.join()
print( '\nBasalt Rock Found.\nAnalysing and Reporting...\nMission Completed' )
time.sleep( 2 )

print( '\nLog\n' )
for val in log:
    print( val )
print( '\nWheel State\n' )
for val in wheelState:
    print( val )
