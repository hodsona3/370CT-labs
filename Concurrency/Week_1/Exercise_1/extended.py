from threading import Thread
import time, math
global pos, lines, perc
lines = []
perc = 0

def printInt( num):
    print( num )

def menu():
  print( "1) Load the poem." )
  var = int(raw_input("Please make your choice: ") )
  if var == 1:
    load = Thread( target=loadThread )
    load.start()
    
def loadThread():
  global perc, lines
  
  with open( '../../caged_bird.txt' ) as my_file:
    lines = my_file.read().splitlines()
  print( "Files Loaded" )
  
  perc = int(raw_input("What percentage of the poem would you like: ") )
  perc = float( perc * len( lines ) / 100.0 )
  perc = math.ceil( perc )
  t.start()

def printFile( threadname ):
    global pos, perc, lines
    print( "Entering Print Thread" )
    perc = int( perc )
    
    for x in range( 0, perc ):
      pos = x
      time.sleep( .3 )
      print( "{}: {}".format( threadname, lines[pos] ) )
        
    print( "{0} exiting\n\r".format( threadname ) )

menuThread = Thread( target=menu() )
menuThread.start()
menuThread.join()
t = Thread(target=printFile, args=( "t" ) )
