from threading import Thread
import time
global pos

def printInt( num):
    print( num )

lines = []
with open( '../../caged_bird.txt' ) as my_file:
  lines = my_file.read().splitlines()


def printFile( threadname ):
    global pos
    
    for x in range( 0, len( lines ) ):
        pos = x
        time.sleep( .5 )
        print( "{}: {}".format( threadname, lines[pos] ) )
        
    print( "{0} exiting\n\r".format( threadname ) )
            
def printFile_2( self, threadname ):
    global pos
    
    for y in range( 0, len( lines ) ):
        pos = y
        time.sleep( .2 )
        
    print( "{0} exiting\n\r".format( threadname ) )


t = Thread(target=printFile, args=( "t" ) )
t2 = Thread( target=printFile_2, args=( "t2" ) )
t.start()
t2.start()
