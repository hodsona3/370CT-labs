from threading import Thread, Condition
import time, threading, random

cv = threading.Condition()

access = 0

def t1( cv, access ):
  access = 1
  condition( access )

def t2( cv, access ):
  access = 2
  condition( access )
  
def condition( access ):
  global lines
  cv.acquire()
  if( access == 1 ):
    print( "Loading file...\n" )
    lines = []
    with open( '../../caged_bird.txt' ) as my_file:
      lines = my_file.read().splitlines()
    time.sleep( 2 )
    cv.notifyAll()
    cv.release()
  else:
    print( "Displaying file...\n" )
    for x in range( len( lines ) ):
      print( lines[x] )
      time.sleep( .2 )
    cv.notifyAll()
    cv.release()

t = Thread(target=t1, args=( cv, access ) )
t2 = Thread( target=t2, args=( cv, access ) )

t.start()
t2.start()