from threading import Thread, Condition
import time, threading, random

cv = threading.Condition()

access = 0

def condition( access ):
  cv.acquire()
  if( access == 1 ):
    print( "\nSection 1." )
    time.sleep(2)
    cv.notifyAll()
    cv.release()

  elif( access == 2 ):
    print( "\nSection 2." )
    time.sleep(2)
    cv.notifyAll()
    cv.release()

  elif( access == 3 ):
    print( "\nSection 3." )
    time.sleep(2)
    cv.notifyAll()
    cv.release()

  else:
    print( "\nSection 4." )
    time.sleep(2)
    cv.notifyAll()
    cv.release()

def t1( cv, access ):
  access = random.randint( 1, 4 )
  condition( access )
  print( "\nThread 1.\n")
  
def t2( cv, access ):
  access = random.randint( 1, 4 )
  condition( access )
  print( "\nThread 2.\n" )
  
def t3( cv, access ):
  access = random.randint( 1, 4 )
  condition( access )
  print( "\nThread 3.\n" )
  
def t4( cv, access ):
  access = random.randint( 1, 4 )
  condition( access )
  print( "\nThread 4.\n" )

t = Thread(target=t1, args=( cv, access ) )
t2 = Thread( target=t2, args=( cv, access ) )
t3 = Thread( target=t3, args=( cv, access ) )
t4 = Thread( target=t4, args=( cv, access ) )

t.start()
t2.start()
t3.start()
t4.start()