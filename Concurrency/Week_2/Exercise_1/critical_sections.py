from threading import Lock, Thread, Condition
import time, random, threading
global pos
cv = threading.Condition()
printLine = True
pos = 0

lines = []
with open( '../../caged_bird.txt' ) as my_file:
  lines = my_file.read().splitlines()

def thread( threadname, printLine, access ):
  global pos
  
  cv.acquire()
  if (access==2):
    print "\nI can do a thing.\n"
    secs = random.randint(1, 4)
    time.sleep(secs)
    cv.notifyAll()
    cv.release()
    access=3
  else:
    if( printLine == True ):
      time.sleep(.1)
      print( "{}".format( lines[pos] ) )
    else:
      time.sleep(.1)
      pos += 1
    cv.notifyAll()
    cv.release()

def printFile( threadname, cv ):
  printLine = True
    
  for x in range( 0, len( lines ) ):
    time.sleep( .1 )
    thread( threadname, printLine, 3 )
      
  print( "{0} exiting\n\r".format( threadname ) )
    

def printFile_2( threadname, cv ):
  printLine = False
  
  for y in range( 0, len( lines ) ):
    time.sleep( .1 )
    thread(  threadname, printLine, 3 )
      
  print( "{0} exiting\n\r".format( threadname ) )
  
def pause( threadname, cv ):
  access = 2
  for z in range( 0, 4 ):
    time.sleep( 2 )
    thread( threadname, False, access )
    
    
t = Thread(target=printFile, args=( "t", cv ) )
t2 = Thread( target=printFile_2, args=( "t2", cv ) )
t3 = Thread( target=pause, args=( "t3", cv ) )
t.start()
t2.start()
t3.start()