Synopsis<br>
========
<br>
This repository covers 3 main topics:<br>
    - Concurrent Programming<br>
    - Parallel Programming<br>
    - Distributed Programming<br>
<br>
Within each section is various tasks relating to the type of programming. These
include locks, condition variables, critical sections and so on.
<br>
<br>
Code Example<br>
=============
<br>
elif( access == 2 ):

    val = solution[ 0 ]
    print( 'Attempting to solve by {}'.format( val ) )
    
    for el in wheel:
        time.sleep( 3 )
        attempt = random.choice( [True, False] )
        
        if( attempt ):
            print( 'Problem solved by Wheel {}'.format( el ) )
    
            log.append( '#{} - Blockage - {} - Solved'.format( len( log ) + 1, val ) )
            
            access = 1
            probSolved = True
        
        else:
            print( 'Problem not solved by Wheel {}'.format( el ) )
            
            log.append( '#{} - Blockage - {} - Not Solved'.format( len( log ) + 1, val ) )
            access = 1
            probSolved = False
            break
    
    time.sleep(2)
    cv.notifyAll()
    cv.release()
<br>
<br>
Motivation<br>
==========
<br>
All the files relate to the 370CT Concurrent and Real-time Systems Design lab 
work.
<br>
<br>
Contributors<br>
============
<br>
Alex Hodson - 5545735