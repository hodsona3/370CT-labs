#include "omp.h"
#include <iostream>
#include <cmath>
#include <cstdlib>

int main (void)
{

int arr[ 10 ] = {2,3,4,5,6,7,8,9,10};
#pragma omp parallel
{
int i = omp_get_thread_num();
//std::cout << omp_get_thread_num() << std::endl;

std::cout << arr[ i ] << std::endl;

}

return 0;
}
